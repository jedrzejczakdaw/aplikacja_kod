FROM openjdk:8-alpine
COPY target/*.jar application.jar
ENTRYPOINT ["java","-jar","/application.jar"]
